package com.classpath.paymentmicroservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Status {
    private Code status;
    private String message;
    public enum Code {
        SUCCESS,
        FAILURE
    }
}
