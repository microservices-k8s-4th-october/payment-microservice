package com.classpath.paymentmicroservice.model;

public enum OrderEventType {
    PENDING,
    CREATED,
    CANCELLED
}
