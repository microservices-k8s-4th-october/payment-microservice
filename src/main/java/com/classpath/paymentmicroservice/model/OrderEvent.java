package com.classpath.paymentmicroservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class OrderEvent {
    private OrderEventType eventType = OrderEventType.PENDING;
    private Order order;
}