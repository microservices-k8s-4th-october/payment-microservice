package com.classpath.paymentmicroservice.controller;

import com.classpath.paymentmicroservice.model.Order;
import com.classpath.paymentmicroservice.model.Status;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import static com.classpath.paymentmicroservice.model.Status.Code.SUCCESS;

@RestController
@RequestMapping("/api/payments")
@Slf4j
@RequiredArgsConstructor
public class PaymentsController {

    private final Environment environment;

    @PostMapping("/{id}")
    public Status acceptPayment(@PathVariable ("id") long orderId, @RequestBody Order order){
      log.info("Inside the payment service :: {}", this.environment.getProperty("server.port"));
      return Status.builder().status(SUCCESS).message("payment is successfull").build();
    }

}