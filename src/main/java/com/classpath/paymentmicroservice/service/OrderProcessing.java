package com.classpath.paymentmicroservice.service;

import com.classpath.paymentmicroservice.model.Order;
import com.classpath.paymentmicroservice.model.OrderEvent;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@Slf4j
public class OrderProcessing {
    @KafkaListener(topics = "order-pradeep-new-topic",groupId = "payment-microservice")
    public void processOrder(String data) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

        // Deserialization into the `orderEvent` class
        OrderEvent orderEvent = objectMapper.readValue(data, OrderEvent.class);
        log.info("processing the orders for :: {}", orderEvent);
    }
}